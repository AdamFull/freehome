#include "nixie_disp.h"

display::display(int DS, int ST_CP, int SH_CP){//setup
  _DS = DS; _ST_CP = ST_CP; _SH_CP = SH_CP;
  _numOfPins = 8;
  pinMode(DS, OUTPUT);
  pinMode(ST_CP, OUTPUT);
  pinMode(SH_CP, OUTPUT);
  delay(100);
  clearRegisters();
}

void display::show(int nums[]) {
  setNumber(nums[0]); setRegisterPin(0, HIGH);
    delay(_delSec); setRegisterPin(0, LOW);

    setNumber(nums[1]); setRegisterPin(1, HIGH);
    delay(_delSec); setRegisterPin(1, LOW);

    setNumber(nums[2]); setRegisterPin(2, HIGH);
    delay(_delSec); setRegisterPin(2, LOW);

    setNumber(nums[3]); setRegisterPin(3, HIGH);
    delay(_delSec); setRegisterPin(3, LOW);
}

void display::setNumber(int num) {
  switch (num)
  {
    case 0:
      setRegisterPin(4, LOW); setRegisterPin(5, LOW);
      setRegisterPin(6, LOW); setRegisterPin(7, LOW);
      break;
    case 1:
      setRegisterPin(4, HIGH); setRegisterPin(5, LOW);
      setRegisterPin(6, LOW);  setRegisterPin(7, LOW);
      break;
    case 2:
      setRegisterPin(4, LOW); setRegisterPin(5, HIGH);
      setRegisterPin(6, LOW); setRegisterPin(7, LOW);
      break;
    case 3:
      setRegisterPin(4, HIGH); setRegisterPin(5, HIGH);
      setRegisterPin(6, LOW);  setRegisterPin(7, LOW);
      break;
    case 4:
      setRegisterPin(4, LOW);  setRegisterPin(5, LOW);
      setRegisterPin(6, HIGH); setRegisterPin(7, LOW);
      break;
    case 5:
      setRegisterPin(4, HIGH); setRegisterPin(5, LOW);
      setRegisterPin(6, HIGH); setRegisterPin(7, LOW);
      break;
    case 6:
      setRegisterPin(4, LOW);  setRegisterPin(5, HIGH);
      setRegisterPin(6, HIGH); setRegisterPin(7, LOW);
      break;
    case 7:
      setRegisterPin(4, HIGH); setRegisterPin(5, HIGH);
      setRegisterPin(6, HIGH); setRegisterPin(7, LOW);
      break;
    case 8:
      setRegisterPin(4, LOW); setRegisterPin(5, LOW);
      setRegisterPin(6, LOW); setRegisterPin(7, HIGH);
      break;
    case 9:
      setRegisterPin(4, HIGH); setRegisterPin(5, LOW);
      setRegisterPin(6, LOW);  setRegisterPin(7, HIGH);
      break;
  }
}

void display::clearRegisters(void)
{
  for(int i = _numOfPins-1; i>=0; i--)
  {
    _registers[i] = LOW;
  }
}

void display::setRegisterPin(int index, boolean data)
{  
  _registers[index] = data;
  writeRegisters();

}

void display::writeRegisters(void)
{
  digitalWrite(_ST_CP, LOW);
  for(int i = _numOfPins-1; i>=0; i--){
    digitalWrite(_SH_CP, LOW);
    boolean data = _registers[i];
    digitalWrite(_DS, data);
    digitalWrite(_SH_CP, HIGH);
  } 
  digitalWrite(_ST_CP, HIGH);
}

void display::setDelay(int delSec) {
  _delSec = delSec;
}

/*
 * !!!!!!!!!!!!!!!!!!!!!!!COMING SOON!!!!!!!!!!!!!!!!!!!!!!!
 */
/*Nixdisp::Nixdisp(int out1, int out2, int out4, int out8) {
  pinMode(out1, OUTPUT); pinMode(out2, OUTPUT);
  pinMode(out4, OUTPUT); pinMode(out8, OUTPUT);
  _out1 = out1; _out2 = out2; _out4 = out4; _out8 = out8;
}
NixSN74HC595::NixSN74HC595(int out1, int out2, int out4, int out8){
  pinMode(out1, OUTPUT); pinMode(out2, OUTPUT);
  pinMode(out4, OUTPUT); pinMode(out8, OUTPUT);
  _out1 = out1; _out2 = out2; _out4 = out4; _out8 = out8;
}*/

/*void Nixdisp::begin(byte segments[]) {
  _segments = sizeof(segments);
  switch (_segments) {
    case 1:
      pinMode(segments[0], OUTPUT);
      _key1 = segments[0];
      break;
    case 2:
      pinMode(segments[0], OUTPUT); pinMode(segments[1], OUTPUT);
      _key1 = segments[0]; _key2 = segments[1];
      break;
    case 3:
      pinMode(segments[0], OUTPUT); pinMode(segments[1], OUTPUT);
      pinMode(segments[2], OUTPUT);
      _key1 = segments[0]; _key2 = segments[1]; _key3 = segments[2];
      break;
    case 4:
      pinMode(segments[0], OUTPUT); pinMode(segments[1], OUTPUT);
      pinMode(segments[2], OUTPUT); pinMode(segments[3], OUTPUT);
      _key1 = segments[0]; _key2 = segments[1]; _key3 = segments[2];
      _key4 = segments[3];
      break;
    case 5:
      pinMode(segments[0], OUTPUT); pinMode(segments[1], OUTPUT);
      pinMode(segments[2], OUTPUT); pinMode(segments[3], OUTPUT);
      pinMode(segments[4], OUTPUT);
      _key1 = segments[0]; _key2 = segments[1]; _key3 = segments[2];
      _key4 = segments[3]; _key5 = segments[4];
      break;
    case 6:
      pinMode(segments[0], OUTPUT); pinMode(segments[1], OUTPUT);
      pinMode(segments[2], OUTPUT); pinMode(segments[3], OUTPUT);
      pinMode(segments[4], OUTPUT); pinMode(segments[5], OUTPUT);
      _key1 = segments[0]; _key2 = segments[1]; _key3 = segments[2];
      _key4 = segments[3]; _key5 = segments[4]; _key6 = segments[5];
      break;
  }
  _startTime = millis();
}

void NixSN74HC595::clearRegisters(void)
{
  for(int i = _numOfPins-1; i>=0; i--)
  {
    _registers[i] = LOW;
  }
}

void NixSN74HC595::begin(int segments, int DS, int ST_CP, int SH_CP, int regsCount){
  _DS = DS; _ST_CP = ST_CP; _SH_CP = SH_CP;
  _numOfPins = regsCount*8;
  _segments = segments; //max 8
  _startTime = millis();
  pinMode(DS, OUTPUT);
  pinMode(ST_CP, OUTPUT);
  pinMode(SH_CP, OUTPUT);
  clearRegisters();
}

void NixSN74HC595::writeRegisters(void)
{
  digitalWrite(_ST_CP, LOW);
  for(int i = _numOfPins-1; i>=0; i--){
    digitalWrite(_SH_CP, LOW);
    boolean data = _registers[i];
    digitalWrite(_DS, data);
    digitalWrite(_SH_CP, HIGH);
  } 
  digitalWrite(_ST_CP, HIGH);
}

void NixSN74HC595::setRegisterPin(int index, boolean data)
{  
  _registers[index] = data;
  writeRegisters();

}

void Nixdisp::setDelay(int delSec) {
  _delSec = delSec;
}
void NixSN74HC595::setDelay(int delSec) {
  _delSec = delSec;
}

void Nixdisp::show(int nums[]) {
  switch (_segments) {
    case 1:
      setNumber(nums[0]); digitalWrite(_key1, HIGH);
      delay(_delSec);     digitalWrite(_key1, LOW);
      break;
    case 2:
      setNumber(nums[0]); digitalWrite(_key1, HIGH);
      delay(_delSec);     digitalWrite(_key1, LOW);

      setNumber(nums[1]); digitalWrite(_key2, HIGH);
      delay(_delSec);     digitalWrite(_key2, LOW);
      break;
    case 3:
      setNumber(nums[0]); digitalWrite(_key1, HIGH);
      delay(_delSec);     digitalWrite(_key1, LOW);

      setNumber(nums[1]); digitalWrite(_key2, HIGH);
      delay(_delSec);     digitalWrite(_key2, LOW);

      setNumber(nums[2]); digitalWrite(_key3, HIGH);
      delay(_delSec);     digitalWrite(_key3, LOW);
      break;
    case 4:
      setNumber(nums[0]); digitalWrite(_key1, HIGH);
      delay(_delSec);     digitalWrite(_key1, LOW);

      setNumber(nums[1]); digitalWrite(_key2, HIGH);
      delay(_delSec);     digitalWrite(_key2, LOW);

      setNumber(nums[2]); digitalWrite(_key3, HIGH);
      delay(_delSec);     digitalWrite(_key3, LOW);

      setNumber(nums[3]); digitalWrite(_key4, HIGH);
      delay(_delSec);     digitalWrite(_key4, LOW);
      break;
    case 5:
      setNumber(nums[0]); digitalWrite(_key1, HIGH);
      delay(_delSec);     digitalWrite(_key1, LOW);

      setNumber(nums[1]); digitalWrite(_key2, HIGH);
      delay(_delSec);     digitalWrite(_key2, LOW);

      setNumber(nums[2]); digitalWrite(_key3, HIGH);
      delay(_delSec);     digitalWrite(_key3, LOW);

      setNumber(nums[3]); digitalWrite(_key4, HIGH);
      delay(_delSec);     digitalWrite(_key4, LOW);

      setNumber(nums[4]); digitalWrite(_key5, HIGH);
      delay(_delSec);     digitalWrite(_key5, LOW);
      break;
    case 6:
      setNumber(nums[0]); digitalWrite(_key1, HIGH);
      delay(_delSec);     digitalWrite(_key1, LOW);

      setNumber(nums[1]); digitalWrite(_key2, HIGH);
      delay(_delSec);     digitalWrite(_key2, LOW);

      setNumber(nums[2]); digitalWrite(_key3, HIGH);
      delay(_delSec);     digitalWrite(_key3, LOW);

      setNumber(nums[3]); digitalWrite(_key4, HIGH);
      delay(_delSec);     digitalWrite(_key4, LOW);

      setNumber(nums[4]); digitalWrite(_key5, HIGH);
      delay(_delSec);     digitalWrite(_key5, LOW);

      setNumber(nums[5]); digitalWrite(_key6, HIGH);
      delay(_delSec);     digitalWrite(_key6, LOW);
      break;
  }
}
void NixSN74HC595::show(int nums[]) {
  switch (_segments) {
    case 1:
    setNumber(nums[0]); setRegisterPin(0, HIGH);
    delay(_delSec); setRegisterPin(0, LOW);
    break;
    case 2:
    setNumber(nums[0]); setRegisterPin(0, HIGH);
    delay(_delSec); setRegisterPin(0, LOW);

    setNumber(nums[1]); setRegisterPin(1, HIGH);
    delay(_delSec); setRegisterPin(1, LOW);
    break;
    case 3:
    setNumber(nums[0]); setRegisterPin(0, HIGH);
    delay(_delSec); setRegisterPin(0, LOW);

    setNumber(nums[1]); setRegisterPin(1, HIGH);
    delay(_delSec); setRegisterPin(1, LOW);

    setNumber(nums[2]); setRegisterPin(2, HIGH);
    delay(_delSec); setRegisterPin(2, LOW);
    break;
    case 4:
    setNumber(nums[0]); setRegisterPin(0, HIGH);
    delay(_delSec); setRegisterPin(0, LOW);

    setNumber(nums[1]); setRegisterPin(1, HIGH);
    delay(_delSec); setRegisterPin(1, LOW);

    setNumber(nums[2]); setRegisterPin(2, HIGH);
    delay(_delSec); setRegisterPin(2, LOW);

    setNumber(nums[3]); setRegisterPin(3, HIGH);
    delay(_delSec); setRegisterPin(3, LOW);
    break;
    case 5:
    setNumber(nums[0]); setRegisterPin(0, HIGH);
    delay(_delSec); setRegisterPin(0, LOW);

    setNumber(nums[1]); setRegisterPin(1, HIGH);
    delay(_delSec); setRegisterPin(1, LOW);

    setNumber(nums[2]); setRegisterPin(2, HIGH);
    delay(_delSec); setRegisterPin(2, LOW);

    setNumber(nums[3]); setRegisterPin(3, HIGH);
    delay(_delSec); setRegisterPin(3, LOW);

    setNumber(nums[4]); setRegisterPin(4, HIGH);
    delay(_delSec); setRegisterPin(4, LOW);
    break;
    case 6:
     setNumber(nums[0]); setRegisterPin(0, HIGH);
    delay(_delSec); setRegisterPin(0, LOW);

    setNumber(nums[1]); setRegisterPin(1, HIGH);
    delay(_delSec); setRegisterPin(1, LOW);

    setNumber(nums[2]); setRegisterPin(2, HIGH);
    delay(_delSec); setRegisterPin(2, LOW);

    setNumber(nums[3]); setRegisterPin(3, HIGH);
    delay(_delSec); setRegisterPin(3, LOW);

    setNumber(nums[4]); setRegisterPin(4, HIGH);
    delay(_delSec); setRegisterPin(4, LOW);

    setNumber(nums[5]); setRegisterPin(5, HIGH);
    delay(_delSec); setRegisterPin(5, LOW);
    break;
    case 7:
    setNumber(nums[0]); setRegisterPin(0, HIGH);
    delay(_delSec); setRegisterPin(0, LOW);

    setNumber(nums[1]); setRegisterPin(1, HIGH);
    delay(_delSec); setRegisterPin(1, LOW);

    setNumber(nums[2]); setRegisterPin(2, HIGH);
    delay(_delSec); setRegisterPin(2, LOW);

    setNumber(nums[3]); setRegisterPin(3, HIGH);
    delay(_delSec); setRegisterPin(3, LOW);

    setNumber(nums[4]); setRegisterPin(4, HIGH);
    delay(_delSec); setRegisterPin(4, LOW);

    setNumber(nums[5]); setRegisterPin(5, HIGH);
    delay(_delSec); setRegisterPin(5, LOW);

    setNumber(nums[6]); setRegisterPin(6, HIGH);
    delay(_delSec); setRegisterPin(6, LOW);
    break;
    case 8:
    setNumber(nums[0]); setRegisterPin(0, HIGH);
    delay(_delSec); setRegisterPin(0, LOW);

    setNumber(nums[1]); setRegisterPin(1, HIGH);
    delay(_delSec); setRegisterPin(1, LOW);

    setNumber(nums[2]); setRegisterPin(2, HIGH);
    delay(_delSec); setRegisterPin(2, LOW);

    setNumber(nums[3]); setRegisterPin(3, HIGH);
    delay(_delSec); setRegisterPin(3, LOW);

    setNumber(nums[4]); setRegisterPin(4, HIGH);
    delay(_delSec); setRegisterPin(4, LOW);

    setNumber(nums[5]); setRegisterPin(5, HIGH);
    delay(_delSec); setRegisterPin(5, LOW);

    setNumber(nums[6]); setRegisterPin(6, HIGH);
    delay(_delSec); setRegisterPin(6, LOW);

    setNumber(nums[7]); setRegisterPin(7, HIGH);
    delay(_delSec); setRegisterPin(7, LOW);
    break;
  }
}

void Nixdisp::setNumber(int num) {
  switch (num)
  {
    case 0:
      digitalWrite (_out1, LOW); digitalWrite (_out2, LOW);
      digitalWrite (_out4, LOW); digitalWrite (_out8, LOW);
      break;
    case 1:
      digitalWrite (_out1, HIGH); digitalWrite (_out2, LOW);
      digitalWrite (_out4, LOW);  digitalWrite (_out8, LOW);
      break;
    case 2:
      digitalWrite (_out1, LOW); digitalWrite (_out2, HIGH);
      digitalWrite (_out4, LOW); digitalWrite (_out8, LOW);
      break;
    case 3:
      digitalWrite (_out1, HIGH); digitalWrite (_out2, HIGH);
      digitalWrite (_out4, LOW);  digitalWrite (_out8, LOW);
      break;
    case 4:
      digitalWrite (_out1, LOW);  digitalWrite (_out2, LOW);
      digitalWrite (_out4, HIGH); digitalWrite (_out8, LOW);
      break;
    case 5:
      digitalWrite (_out1, HIGH); digitalWrite (_out2, LOW);
      digitalWrite (_out4, HIGH); digitalWrite (_out8, LOW);
      break;
    case 6:
      digitalWrite (_out1, LOW);  digitalWrite (_out2, HIGH);
      digitalWrite (_out4, HIGH); digitalWrite (_out8, LOW);
      break;
    case 7:
      digitalWrite (_out1, HIGH); digitalWrite (_out2, HIGH);
      digitalWrite (_out4, HIGH); digitalWrite (_out8, LOW);
      break;
    case 8:
      digitalWrite (_out1, LOW); digitalWrite (_out2, LOW);
      digitalWrite (_out4, LOW); digitalWrite (_out8, HIGH);
      break;
    case 9:
      digitalWrite (_out1, HIGH); digitalWrite (_out2, LOW);
      digitalWrite (_out4, LOW);  digitalWrite (_out8, HIGH);
      break;
  }
}
void NixSN74HC595::setNumber(int num) {
  switch (num)
  {
    case 0:
      digitalWrite (_out1, LOW); digitalWrite (_out2, LOW);
      digitalWrite (_out4, LOW); digitalWrite (_out8, LOW);
      break;
    case 1:
      digitalWrite (_out1, HIGH); digitalWrite (_out2, LOW);
      digitalWrite (_out4, LOW);  digitalWrite (_out8, LOW);
      break;
    case 2:
      digitalWrite (_out1, LOW); digitalWrite (_out2, HIGH);
      digitalWrite (_out4, LOW); digitalWrite (_out8, LOW);
      break;
    case 3:
      digitalWrite (_out1, HIGH); digitalWrite (_out2, HIGH);
      digitalWrite (_out4, LOW);  digitalWrite (_out8, LOW);
      break;
    case 4:
      digitalWrite (_out1, LOW);  digitalWrite (_out2, LOW);
      digitalWrite (_out4, HIGH); digitalWrite (_out8, LOW);
      break;
    case 5:
      digitalWrite (_out1, HIGH); digitalWrite (_out2, LOW);
      digitalWrite (_out4, HIGH); digitalWrite (_out8, LOW);
      break;
    case 6:
      digitalWrite (_out1, LOW);  digitalWrite (_out2, HIGH);
      digitalWrite (_out4, HIGH); digitalWrite (_out8, LOW);
      break;
    case 7:
      digitalWrite (_out1, HIGH); digitalWrite (_out2, HIGH);
      digitalWrite (_out4, HIGH); digitalWrite (_out8, LOW);
      break;
    case 8:
      digitalWrite (_out1, LOW); digitalWrite (_out2, LOW);
      digitalWrite (_out4, LOW); digitalWrite (_out8, HIGH);
      break;
    case 9:
      digitalWrite (_out1, HIGH); digitalWrite (_out2, LOW);
      digitalWrite (_out4, LOW);  digitalWrite (_out8, HIGH);
      break;
  }
}*/

