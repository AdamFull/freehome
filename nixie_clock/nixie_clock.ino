#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>

/*!!!!!!!!ВНИМАНИЕ!!!!!!!!
 * После первого перезапуска нужно перезапустить  ESP вручную!
 */

// |======================================================================================================|
// |В ПЛАНАХ:                                                                                             |
// |-Синхронизация с другой точкой доступа(Перенести получение информации на ESP32) (В ПЛАНАХ)            |
// |-Многоуровневый будильник (В ПЛАНАХ)                                                                  |
// |-Подстройка под нужное число ламп (В ПЛАНАХ)                                                          |
// |-Музыка из интернета (В ПЛАНАХ) - програмно трудно, а ЦАП ждать долго                                 |
// |-Вывод файлов из будильника на сайт (В ПЛАНАХ)                                                        |
// |-Управление через MQTT интерфейс - с любой точки мира.                                                |
// |ЗАВЕРШЕНО:                                                                                            |
// |-Адаптивный веб-интерфейс (ТРЕБУЕТ ДОРАБОТКИ)                                                         |
// |-При первом включении запускать сервер с настройками (ЗАВЕРШЕНО)                                      |
// |-Будильник (РАБОТАЕТ НЕКОРРЕКТНО)                                                                     |
// |-Получение погоды (ЗАВЕРШЕНО) - погода в веб интерфейсе                                               |
// |-Перезагрузка через интерфейс (РАБОТАЕТ НЕКОРРЕКТНО)                                                  |
// |-Вывод чисел (ЗАВЕРШЕНО)                                                                              |
// |-Практически полная настройка через веб-интерфейс (ЗАВЕРШЕНО)                                         |
// |ПРОБЛЕМЫ:                                                                                             |
// |-Прошивка по воздуху - (ПРОБЛЕМЫ С РЕБУТОМ)                                                           |
// |-Ускорение загрузки веб-страницы (ВЫСОКИЕ ТАЙМИНГИ)                                                   |
// |-Настройка генератора случайных чисел для будильника (ВЫДАЁТ ОДНО И ТО ЖЕ ЧИСЛО)                      |
// |======================================================================================================|

char *_SSDP_name;
char *_ssid;  //  your network SSID (name) RT-GPON-80AE
char *_pass;       // your network password R3FC7XDA
String _weatherKey; //eac0b755302dadb29a35aec3273c271b
String _weatherLang; //&lang=ru
String _cityID; //472045
int _utc; //3
String _ip;
String jsonConfig ="{}";
int del;
long updateInf;

String _name[] = {"", "", "", "", "", "", ""};
bool _active[] = {false, false, false, false, false, false, false};
bool _repeat[] = {false, false, false, false, false, false, false};
String _time[] = {"", "", "", "", "", "", ""};

String weekday;
int _volume = 27;
bool _random = false;

int alarm_duration = 320;




#include "nixie_disp.h"
#include "alarm.h"
#include "settings.h"

// =======================================================================
// Конфигурация устройства:
// =======================================================================

WiFiClient client;
display nLamps(D7, D8, D5);
ServerH srvh;
Alarm alarm;

bool debug = true;

String weatherMain = "";
String weatherDescription = "";
String weatherLocation = "";
String country;
int humidity;
int pressure;
float temp;
float tempMin, tempMax;
int clouds;
float windSpeed;
String date;
String weatherString;

int offset = 1, refresh = 0;

void setup(void)
{  
  if(debug)Serial.begin(115200);                        // Дебаг
  pinMode(D4, OUTPUT);
  digitalWrite(D4,1);
  delay(2000);
  ESP.wdtDisable(); //disable watchdog
  ESP.wdtEnable(WDTO_8S);
  Serial.println("Init watchdog:");
  for (long x = 0; x<100; x++){
    digitalWrite(D4, !digitalRead(D4));
    Serial.print(".");
    delay(50);
    yield(); //and start watchdog
    ESP.wdtFeed();
  }
  Serial.println();
  Serial.println("Watchdog ok!");
  nLamps.setDelay(del);
  srvh.setup();
  delay(300);
  srvh.startHttp();
  delay(300);
  alarm.start();
}

// =======================================================================
long dotTime = 0;
long clkTime = 0;
int h, m, s;
// =======================================================================

const char *weatherHost = "api.openweathermap.org";

void getWeatherData()
{
  Serial.print("connecting to "); Serial.println(weatherHost);
  if (client.connect(weatherHost, 80)) {
    client.println(String("GET /data/2.5/weather?id=") + _cityID + "&units=metric&appid=" + _weatherKey + "&lang=" +  _weatherLang + "\r\n" +
                   "Host: " + weatherHost + "\r\nUser-Agent: ArduinoWiFi/1.1\r\n" +
                   "Connection: close\r\n\r\n");
  } else {
    Serial.println("connection failed");
    return;
  }
  String line;
  int repeatCounter = 0;
  while (!client.available() && repeatCounter < 10) {
    delay(500);
    Serial.println("w.");
    repeatCounter++;
  }
  while (client.connected() && client.available()) {
    char c = client.read();
    if (c == '[' || c == ']') c = ' ';
    line += c;
  }

  client.stop();

  DynamicJsonBuffer jsonBuf;
  JsonObject &root = jsonBuf.parseObject(line);
  if (!root.success())
  {
    Serial.println("parseObject() failed");
    return;
  }
  //weatherMain = root["weather"]["main"].as<String>();
  weatherDescription = root["weather"]["description"].as<String>();
  weatherDescription.toLowerCase();
  //  weatherLocation = root["name"].as<String>();
  //  country = root["sys"]["country"].as<String>();
  temp = root["main"]["temp"];
  humidity = root["main"]["humidity"];
  pressure = root["main"]["pressure"];
  tempMin = root["main"]["temp_min"];
  tempMax = root["main"]["temp_max"];
  windSpeed = root["wind"]["speed"];
  clouds = root["clouds"]["all"];
  String deg = String(char('~' + 25));
  weatherString = "t: " + String(temp, 1) + " ";
  weatherString += weatherDescription;
  weatherString += " Влажн.: " + String(humidity) + "% ";
  weatherString += "Давл.: " + String(pressure / 1.3332239) + " мм ";
  //  weatherString += "Облачность: " + String(clouds) + "% ";
  weatherString += "Ветер: " + String(windSpeed, 1) + "М/с";
}

// =======================================================================
// Берем время у GOOGLE
// =======================================================================


long localEpoc = 0;
long localMillisAtUpdate = 0;

void getTime()
{
  WiFiClient client;
  if (!client.connect("www.google.com", 80)) {
    Serial.println("connection to google failed");
    return;
  }

  client.print(String("GET / HTTP/1.1\r\n") +
               String("Host: www.google.com\r\n") +
               String("Connection: close\r\n\r\n"));
  int repeatCounter = 0;
  while (!client.available() && repeatCounter < 10) {
    delay(500);
    Serial.println(".");
    repeatCounter++;
  }

  String line;
  client.setNoDelay(false);
  while (client.connected() && client.available()) {
    line = client.readStringUntil('\n');
    line.toUpperCase();
    if (line.startsWith("DATE: ")) {
      date = "     " + line.substring(6, 23);
      weekday = "" + line.substring(6, 9);
      h = line.substring(23, 25).toInt();
      m = line.substring(26, 28).toInt();
      s = line.substring(29, 31).toInt();
      localMillisAtUpdate = millis();
      localEpoc = (h * 60 * 60 + m * 60 + s);
    }
  }
  client.stop();
}

// =======================================================================r

void updateTime()
{
  long curEpoch = localEpoc + ((millis() - localMillisAtUpdate) / 1000);
  long epoch = round(curEpoch + 3600 * _utc + 86400L) % 86400L;
  h = ((epoch  % 86400L) / 3600) % 24;
  m = (epoch % 3600) / 60;
  s = epoch % 60;
}

// =======================================================================

int updCnt = 0;

void loop(void)
{
  srvh.handle();
  if(updCnt <= 0){
    updCnt = 10;
    Serial.println(updateInf);
    clkTime = millis();
    Serial.println("Getting data ...");
    getWeatherData();
    getTime();
    Serial.println("Data loaded");
  }
  if (millis() - clkTime > (updateInf * 60000)) { // добавить сохранение параметров сюда
    clkTime = millis();
    Serial.println("Saving data ...");
    srvh.save();
    Serial.println("Data saved");
    Serial.println("Getting data ...");
    getWeatherData();
    getTime();
    Serial.println("Data loaded");
    
  }
  if (millis() - dotTime > 1000) {
    dotTime = millis();
    Serial.println(String(h) + ":" + String(m) + ":" + String(s) + "  date: " + date);
    Serial.println(weatherString);
    digitalWrite(D4, !digitalRead(D4)); //you can connect dots to that pin (GPIO2)
    alarm.handle(String(h), String(m), weekday);
     //While the LED D4 blinks, esp understands that everything is fine and can continue to wor;
    ESP.wdtFeed();
  }

  alarm.loop();
  int nums[] = {h / 10, h % 10, m / 10, m % 10};
  nLamps.show(nums);
  updateTime();

}

// =======================================================================
// Берем погоду с сайта openweathermap.org
// =======================================================================


