#include "settings.h"

ESP8266WebServer server(80);
File fsUploadF;

ServerH::ServerH() {
  //SSDP_init(_SSDP_name);
}
void saveConfig(){
      // Резервируем памяь для json обекта буфер может рости по мере необходимти предпочтительно для ESP8266 
      DynamicJsonBuffer jsonBuffer;
      JsonObject& json = jsonBuffer.parseObject(jsonConfig);
      json["SSDP"] = _SSDP_name;
      json["ssid"] = _ssid;
      json["ssidpass"] = _pass;
      json["apiCode"] = _weatherKey;
      json["country"] = _cityID;
      json["lang"] = _weatherLang;
      json["utc"] = _utc;
      json["ip"] = _ip;
      json.printTo(jsonConfig);
      File configFile = SPIFFS.open("/config.json", "w");
      if (!configFile) {
        
      }
      json.printTo(configFile);
      configFile.close();
}

void loadAlarms(){
  File alarmsFile = SPIFFS.open("/alarms.json", "r");
  size_t size = alarmsFile.size();
  if (size > 1024) {
    Serial.println("Alarm file size is too large");
  }
  String jsonAlarms = alarmsFile.readString();
  DynamicJsonBuffer jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(jsonAlarms);
  String day;
  for(int i = 0; i < 7; i++){
    switch(i){
      case 0: day = "Monday"; break;
      case 1: day = "Tuesday"; break;
      case 2: day = "Wednesday"; break;
      case 3: day = "Thursday"; break;
      case 4: day = "Friday"; break;
      case 5: day = "Saturday"; break;
      case 6: day = "Sunday"; break;
    }
    _name[i] = root[day]["name"].as<String>();
    _active[i] = root[day]["active"].as<bool>();
    _repeat[i] = root[day]["repeat"].as<bool>();
    _time[i] = root[day]["time"].as<String>();
  }
  alarmsFile.close();
}

void saveAlarm(){
  File alarmFile = SPIFFS.open("/alarms.json", "r");
  String jsonAlarms = alarmFile.readString();
  alarmFile.close();
  DynamicJsonBuffer jsonBuffer;
  JsonObject& json = jsonBuffer.parseObject(jsonAlarms);
  String day;
  for(int i = 0; i < 7; i++){
    switch(i){
      case 0: day = "Monday"; break;
      case 1: day = "Tuesday"; break;
      case 2: day = "Wednesday"; break;
      case 3: day = "Thursday"; break;
      case 4: day = "Friday"; break;
      case 5: day = "Saturday"; break;
      case 6: day = "Sunday"; break;
    }
    json[day]["name"] = _name[i];
    json[day]["active"] = _active[i];
    json[day]["repeat"] = _repeat[i];
    json[day]["time"] = _time[i];
  }

   json.printTo(jsonAlarms);
   File configFile = SPIFFS.open("/alarms.json", "w");
   json.printTo(configFile);
   configFile.close();
   loadAlarms();
}

void ServerH::save(){
  saveAlarm();
  saveConfig();
}

void ServerH::load() {
  File configFile = SPIFFS.open("/config.json", "r");
  size_t size = configFile.size();
  if (size > 1024) {
    Serial.println("Config file size is too large");
  }
  jsonConfig = configFile.readString();
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(jsonConfig);
    _SSDP_name = strdup(root["SSDP"].as<String>().c_str());
    _ssid = strdup(root["ssid"].as<String>().c_str());
    _pass = strdup(root["ssidpass"].as<String>().c_str());
    _weatherKey = root["apiCode"].as<String>();
    _cityID = root["country"].as<String>();
    _weatherLang = root["lang"].as<String>();
    _utc = root["utc"];
    _volume = root["vol"];
    del = root["delay"];
    updateInf = root["upd"];
    configFile.close();
    loadAlarms();
}

void handle_almsts(){
  _volume = atoi(server.arg("vol").c_str());
}

void handle_alarmSet(){
  int i = atoi(server.arg("day").c_str());
  _name[i] = server.arg("name");
  if(server.arg("active") == "true"){_active[i] = true;}
  else{ _active[i] = false;}
  if(server.arg("repeat") == "true"){_repeat[i] = true;}
  else{_repeat[i] = false;}
  _time[i] = server.arg("time");
  saveAlarm();
}

void handle_alarmReset(){
  int i = atoi(server.arg("day").c_str());
  _name[i] = server.arg("name");
  _active[i] = server.arg("active");
  _repeat[i] = server.arg("repeat");
  _time[i] = server.arg("time");
  saveAlarm();
}

void handle_ssid(){
  _ssid = strdup(server.arg("ssid").c_str());
  _pass = strdup(server.arg("password").c_str());
  saveConfig();
}

void handle_weather(){
  _weatherKey = server.arg("api");
  _cityID = server.arg("city");
  _weatherLang = server.arg("lang");
  _utc = atoi(server.arg("utc").c_str());
  saveConfig();
}

void handle_WeatherJSON(){
  String json = "{";
  json += "\"temperature\":\"";
  json += temp;
  json += "\",\"humidity\":\"";
  json += humidity;
  json += "\",\"pressure\":\"";
  json += pressure / 1.3332239;
  json += "\",\"wind\":\"";
  json += windSpeed;
  json += "\"}";
  server.send(200, "text/json", json);
}

void handle_Restart() {
  String restart = server.arg("device");          // Получаем значение device из запроса
  if (restart == "ok") {                         // Если значение равно Ок
    server.send(200, "text / plain", "Reset OK"); // Oтправляем ответ Reset OK
    digitalWrite(D4,0);
    WiFi.forceSleepBegin();
    wdt_reset();
    ESP.restart();
  }
  else {                                        // иначе
    server.send(200, "text / plain", "No Reset"); // Oтправляем ответ No Reset
  }
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void FS_init(){
  if (!SPIFFS.begin())
  {
    // Serious problem
    Serial.println("SPIFFS Mount failed");
  } else {
    Serial.println("SPIFFS Mount succesfull");
  }
  server.serveStatic("/css", SPIFFS, "/css");
  server.serveStatic("/js", SPIFFS, "/js");
  server.serveStatic("/", SPIFFS, "/index.html");
  server.serveStatic("/favicon.ico", SPIFFS, "/favicon.ico");
  server.serveStatic("/config.json", SPIFFS, "/config.json");
  server.serveStatic("/alarms.json", SPIFFS, "/alarms.json");
}

void SSDP_init() {
  server.on("/description.xml", HTTP_GET, []() {
    SSDP.schema(server.client());
  });
  SSDP.setSchemaURL("description.xml");
  SSDP.setHTTPPort(80);
  SSDP.setName(_SSDP_name);
  SSDP.setSerialNumber("000156324589");
  SSDP.setURL("/");
  SSDP.setModelName("Nixie Clock");
  SSDP.setModelNumber("0.1");
  SSDP.setModelURL("");
  SSDP.setManufacturer("Ilia Baratoff");
  SSDP.setManufacturerURL("hhtp://www.vk.com/logotipick");
  SSDP.begin();
}


void ServerH::setup() {
  FS_init();
  delay(500);
  load();
  delay(500);
  WiFi.mode(WIFI_STA);
  byte tries = 11;
  WiFi.begin(_ssid, _pass);
  while (--tries && WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.println("Connected to ");
  Serial.print(WiFi.SSID());
  Serial.println();
  Serial.println(WiFi.localIP());
  if (WiFi.status() != WL_CONNECTED) {
    WiFi.disconnect();
    WiFi.mode(WIFI_AP);
    WiFi.softAP(_SSDP_name, _pass);
    IPAddress myIP = WiFi.softAPIP();
  }
  delay(500);
  SSDP_init();
}

void ServerH::startHttp() {
  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/inline", []() {
    server.send(200, "text/plain", "this works as well");
  });

  server.on("/ssid", handle_ssid);
  server.on("/alarmSets", handle_almsts);
  server.on("/weather", handle_weather);
  server.on("/restart", handle_Restart);
  server.on("/alarmUpd", handle_alarmSet);
  server.on("/alarmDel", handle_alarmReset);
  server.on("/weather.json", handle_WeatherJSON);
  server.onNotFound(handleNotFound);
 
  server.begin();
}



void ServerH::handle() {
  server.handleClient();
}

