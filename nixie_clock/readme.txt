                                                                //README EN//
This project is part of a larger project of a smart home. At the moment, this watch contains most of the functionality,
which will be further distributed to other devices.

The program code is fully open. Use, do, edit at your own discretion.

Before flashing the esp itself, you need to flash its kernel.
I had a nucleus of 2013, and the sketch always went into rebut, stupidly worked.
I was looking for this solution for about 2 days, and eventually found it.
The kernel itself contains the initialization of the ESC and AT commands.

Instructions for the kernel firmware:
1) First, download the latest version of the python from the official site. (Https://www.python.org/downloads/)
2) Then we need a program that essentially stitches esp, but in my case it gave an error in the encoding
and we use it to determine the configuration of the CAP. (https://www.espressif.com/en/support/download/other-tools version 3.6.4)
-Get the program
-Choose item with ESP8266
-In the window that opens, select the port to which the ESP is connected and set the speed to 115200.
-Transpose esp into bootloader mode (Press reset, hold flash, release reset, release flash).
- Press the start button.
-To the right we will see the text area in which our data, we need what is written after QUAD (I have 32 Mbit = 4096 Kbytes)
-Next you should know that ESP-01 has a SPI frequency of 40 MHz, for ESP-07, -12, -12E 80 MHz, ESP32 160 MHz.

3) Download NFP loader working on pascal https://github.com/nodemcu/nodemcu-flasher
4) Download the latest version of the kernel https://www.espressif.com/en/support/download/at?keys=&field_type_tid%5B%5D=14 (at the time of the last version was 1.6.1)
5) In the downloaded program
-In the first tab, select the com port
-In the second tab, remove all the checkmarks
-In the third tab buidrate set 115200, Flash size (step 2), flash speed (Step 2), SPI mode: DIO.
-Come on the first tab and click on Flash, if everything is successful, pop-up addresses will appear and in the bottom left corner there will be a checkmark
-Support the archive with the firmware wherever you are comfortable.
-In the downloaded archive with the firmware in the folder / bin / at there is a file README.md in it we find our configuration, in my case Flash size 32Mbit: 512KB + 512KB (ESP-12E)
- Again, go to the second tab and put a tick in the first four fields.
-Fields are filled in according to README.md
-After selecting all the files and setting the byte, go back to the first tab and press Flash.
-If everything went well, there will be a tick in the lower left corner.
-Turn off the power of the device and turn it on again.

6) Now we have to check everything we did right, for this we download the program http://www.npackd.org/p/com.com/puphase.Termite/3.3
7) Run the program on behalf of the administrator (Program for UART RS232 controllers)
8) After login, we need to configure it
-Open Settings
-Choose a com port
-Select the port speed (115200)
-Choose Add CR + LF
-Pull the checkmark with Local Echo
-We'll tick Word Wrap
-Please tick off the Autocomplete edit line
-Click OK
9) Well and finally check. After the program reboots, enter AT. If we get OK, the AT commands were loaded successfully.
And we introduce AT + GMR. If all the information about the firmware is displayed, then the installation was successful!
10) Rejoice and put sketches.

Instructions for installing the firmware:
1) Install an additional file on Arduino ide.
2) Open the ide project file.
3) Go to the "Tools" tab and click "ESP8266 Sketch data upload"
4) Load the sketch into your esp.
5) Go to the access point that appears and set up the clock.
6) After the first reboot through the interface, a manual reboot is required.
7) Done!


                                                          //README RUS//
Данный проект является частью более масштабного проекта умного дома. На данный момент эти часы содержат большую часть всего функционала, 
который в дальнейшем будет распределён на другие устройства.

Программный код полностью открыт. Пользуйтесь, делайте, редактируйте на своё усмотрение.

Перед тем как прошивать саму esp, нужно прошить её ядро.
У меня стояло ядро 2013 года, и скетч постоянно уходил в ребут, тупо нестабильно работал.
Я искал это решение около 2х дней, и в конце концов нашёл.
Само ядро содержит инициализацию есп и АТ команды.

Инструкция по прошивке ядра:
1)Для начала скачайте последнюю версию питона с оффициального сайта.(https://www.python.org/downloads/)
2)Потом нам нужна будет программа, которая по сути прошивает есп, но в моём случае она выдавала ошибку в кодировке
и её мы используем для определения конфигурации ЕСП. (https://www.espressif.com/en/support/download/other-tools версия 3.6.4)
-Запускаем программу
-Выбираем пункт с ESP8266
-В открывшемся окне выбираем порт к которому подключена есп и скорость ставим 115200.
-Переводим есп в режим бутлоадера(Зажать ресет, зажать флэш, отпустить ресет, отпустить флэш).
-Жмём кнопку старт.
-Справа мы увидим текстовую область в которой наши данные, нам нужно то что написано после QUAD (У меня 32 Мбит = 4096 Кбайт)
-Далее вы должны знать что у ESP-01 частота SPI 40 МГц, у ESP-07,-12,-12E 80 МГц, ESP32 160 МГц.

3)Качаем загрузчик NFP работающий на паскале https://github.com/nodemcu/nodemcu-flasher
4)Качаем последнюю версию ядра https://www.espressif.com/en/support/download/at?keys=&field_type_tid%5B%5D=14 (на момент создания последней была 1.6.1)
5)В скачаной программе
-В первой вкладке выбираем ком порт
-Во второй вкладке убираем все галочки
-В третьей вкладке buidrate ставим 115200, Flash size (шаг 2), flash speed (Шаг 2), SPI mode: DIO.
-Переходим на первую вкладку и нажимаем Flash, если всё успешно, то появятся мак адреса и в нижнем левом углу будет галочка
-Распакуем архив с прошивкой куда вам удобно.
-В скачаном архиве с прошивкой в папке /bin/at есть файл README.md в нём находим свою конфигурацию, в моём случае Flash size 32Mbit: 512KB+512KB (ESP-12E)
-Снова переходим во вторую вкладку и ставим галочки на первых четырёх полях.
-Поля заполняем согласно README.md
-После выбора всех файлов и установки байт снова переходим в первую вкладку и нажимаем Flash.
-Если всё прошло успешно то в нижнем левом углу будет галочка.
-Отключаем питание устройства и включаем снова.

6)Теперь мы должны проверить всё ли правильно мы сделали, для этого качаем программу http://www.npackd.org/p/com.compuphase.Termite/3.3
7)Запускаем программу от имени администратора(Программа для контроллеров UART RS232)
8)После входа мы должны настроить его
-Открываем Settings
-Выбираем ком порт
-Выбираем скорость порта(115200)
-Выбираем Add CR+LF
-Убираем галочку с Local Echo
-Ставим галочку на Word Wrap
-Убираем галочку с Autocomplete edit line
-Нажимаем ОК
9)Ну и наконец проверка. После того как программа перезагрузится, вводим AT. Если получаем OK, то АТ команды были загружены успешно.
И вводим AT+GMR. Если выведется вся информация об прошивке то установка была произведена успешно!
10)Радуйтесь и ставьте скетчи.

Инструкция по установке прошивки: 
1) Устанавливаем на Arduino ide дополнительный файл.
2) Открываем в ide файл проекта.
3) Заходим в вкладку "инструменты" и нажимаем "ESP8266 Sketch data upload"
4) Загружаем скетч в вашу esp.
5) Заходим в появившуюся точку доступа и настраиваем часы.
6) После первой перезагрузки через интерфейс, требуется ручная перезагрузка.
7) Готово!
