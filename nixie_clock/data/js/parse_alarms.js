$('document').ready(function(){

    function populate(data) {
            $.each(data, function(key, value){
                $('[name='+key+']', '#tableWeather').html(value);
            });
        }
        $.getJSON('weather.json', function(object) {
            populate($.parseJSON(JSON.stringify(object)));
        });

            function getalms(data) {
              $.each(data.Monday, function(key, value){
                    $('[name='+key+']', '#Monday').html(value);
                    if(key == "active" && value == true) $('[name=activetb]', '#Monday').addClass("bg-success");
                    if(key == "repeat" && value == true) $('[name=repeattb]', '#Monday').addClass("bg-success");
                    if(key == "active" && value == false) $('[name=activetb]', '#Monday').addClass("bg-danger");
                    if(key == "repeat" && value == false) $('[name=repeattb]', '#Monday').addClass("bg-danger");
                });
                $.each(data.Tuesday, function(key, value){
                    $('[name='+key+']', '#Tuesday').html(value);
                    if(key == "active" && value == true) $('[name=activetb]', '#Tuesday').addClass("bg-success");
                    if(key == "repeat" && value == true) $('[name=repeattb]', '#Tuesday').addClass("bg-success");
                    if(key == "active" && value == false) $('[name=activetb]', '#Tuesday').addClass("bg-danger");
                    if(key == "repeat" && value == false) $('[name=repeattb]', '#Tuesday').addClass("bg-danger");
                });
                $.each(data.Wednesday, function(key, value){
                    $('[name='+key+']', '#Wednesday').html(value);
                    if(key == "active" && value == true) $('[name=activetb]', '#Wednesday').addClass("bg-success");
                    if(key == "repeat" && value == true) $('[name=repeattb]', '#Wednesday').addClass("bg-success");
                    if(key == "active" && value == false) $('[name=activetb]', '#Wednesday').addClass("bg-danger");
                    if(key == "repeat" && value == false) $('[name=repeattb]', '#Wednesday').addClass("bg-danger");
                });
                $.each(data.Thursday, function(key, value){
                    $('[name='+key+']', '#Thursday').html(value);
                    if(key == "active" && value == true) $('[name=activetb]', '#Thursday').addClass("bg-success");
                    if(key == "repeat" && value == true) $('[name=repeattb]', '#Thursday').addClass("bg-success");
                    if(key == "active" && value == false) $('[name=activetb]', '#Thursday').addClass("bg-danger");
                    if(key == "repeat" && value == false) $('[name=repeattb]', '#Thursday').addClass("bg-danger");
                });
                $.each(data.Friday, function(key, value){
                    $('[name='+key+']', '#Friday').html(value);
                    if(key == "active" && value == true) $('[name=activetb]', '#Friday').addClass("bg-success");
                    if(key == "repeat" && value == true) $('[name=repeattb]', '#Friday').addClass("bg-success");
                    if(key == "active" && value == false) $('[name=activetb]', '#Friday').addClass("bg-danger");
                    if(key == "repeat" && value == false) $('[name=repeattb]', '#Friday').addClass("bg-danger");
                });
                $.each(data.Saturday, function(key, value){
                    $('[name='+key+']', '#Saturday').html(value);
                    if(key == "active" && value == true) $('[name=activetb]', '#Saturday').addClass("bg-success");
                    if(key == "repeat" && value == true) $('[name=repeattb]', '#Saturday').addClass("bg-success");
                    if(key == "active" && value == false) $('[name=activetb]', '#Saturday').addClass("bg-danger");
                    if(key == "repeat" && value == false) $('[name=repeattb]', '#Saturday').addClass("bg-danger");
                });
                $.each(data.Sunday, function(key, value){
                    $('[name='+key+']', '#Sunday').html(value);
                    if(key == "active" && value == true) $('[name=activetb]', '#Sunday').addClass("bg-success");
                    if(key == "repeat" && value == true) $('[name=repeattb]', '#Sunday').addClass("bg-success");
                    if(key == "active" && value == false) $('[name=activetb]', '#Sunday').addClass("bg-danger");
                    if(key == "repeat" && value == false) $('[name=repeattb]', '#Sunday').addClass("bg-danger");
                });
            }
            $.getJSON('alarms.json', function(object) {
                  getalms($.parseJSON(JSON.stringify(object)));
              });

            $('#day').change(function(){
                day = $(this).val();
                function getalms(data) {
                    switch(day){
                        case "0":
                        $.each(data.Monday, function(key, value){
                            $('[name='+key+']', '#alarms').val(value);
                            if(key === "active"){
                                $('#active').prop('checked', value);
                            }
                            if(key === "repeat"){
                             $('#repeat').prop('checked', value);
                            }
                        });
                        break;
                        case "1":
                        $.each(data.Tuesday, function(key, value){
                            $('[name='+key+']', '#alarms').val(value);
                            if(key === "active"){
                                $('#active').prop('checked', value);
                            }
                         if(key === "repeat"){
                                $('#repeat').prop('checked', value);
                            }
                        });
                        break;
                        case "2":
                        $.each(data.Wednesday, function(key, value){
                            $('[name='+key+']', '#alarms').val(value);
                            if(key === "active"){
                                $('#active').prop('checked', value);
                            }
                            if(key === "repeat"){
                                $('#repeat').prop('checked', value);
                            }
                        });
                        break;
                        case "3":
                        $.each(data.Thursday, function(key, value){
                            $('[name='+key+']', '#alarms').val(value);
                           if(key === "active"){
                                $('#active').prop('checked', value);
                            }
                            if(key === "repeat"){
                            $('#repeat').prop('checked', value);
                            }
                        });
                        break;
                        case "4":
                        $.each(data.Friday, function(key, value){
                            $('[name='+key+']', '#alarms').val(value);
                            if(key === "active"){
                                $('#active').prop('checked', value);
                            }
                            if(key === "repeat"){
                                $('#repeat').prop('checked', value);
                            }
                        });
                        break;
                        case "5":
                        $.each(data.Saturday, function(key, value){
                            $('[name='+key+']', '#alarms').val(value);
                            if(key === "active"){
                                $('#active').prop('checked', value);
                            }
                            if(key === "repeat"){
                                $('#repeat').prop('checked', value);
                            }
                        });
                        break;
                        case "6":
                        $.each(data.Sunday, function(key, value){
                            $('[name='+key+']', '#alarms').val(value);
                            if(key === "active"){
                                $('#active').prop('checked', value);
                            }
                            if(key === "repeat"){
                                $('#repeat').prop('checked', value);
                            }
                        });
                        break;
                    }
            }
                $.getJSON('alarms.json', function(object) {
                    getalms($.parseJSON(JSON.stringify(object)));
                });
            });
          });

