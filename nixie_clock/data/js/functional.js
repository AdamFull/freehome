$('document').ready(function(){
		function populate(data) {
    		$.each(data, function(key, value){
      			$('[name='+key+']', '#settings').val(value);
            $('[name='+key+']', '#tableData').html("<small>" + value + "</small>");
    		});
  		}
    	$.getJSON('config.json', function(object) {
    	  	populate($.parseJSON(JSON.stringify(object)));
    	});

    	
      function updateUrl(){
        location.reload();
      }

		$.ajaxSetup({
			global: false,
			type: "GET"
		});

		$('#reboot').click(function() {
			server = "device=ok";
			$.ajax({
        url: "/restart",
				data: server
			});
      alert("Rebooting... The page will be updated when the changes take effect.");
      setTimeout(updateUrl, 8000);
				return false;
    	});
			
		$('#ssidGet').click(function() {
			server = "ssid=" + $("#ssid").val() + "&password=" + $("#password").val();
			$.ajax({
				url: "/ssid",
				data: server
			});
			$('#ssidResult').show(500);
			$('#ssidGet').prop("disabled", true);
			return false;
    	});
    	$('#apiGet').click(function() {
			server = "api=" + $("#apiCode").val() + "&city=" + $("#cityCode").val() + "&lang=" + $("#timelang").val() + "&utc=" + $("#timestamp").val();
			$.ajax({
				url: "/weather",
				data: server
			});
			$('#apiGet').prop("disabled", true);
			return false;
    	});
    	$('#otherGet').click(function(){
    		server = "SSDP=" + $("#ssdp").val() + "&delay=" + $("#segdelay").val() + "&upd=" + $("#updrate").val();
			$.ajax({
        url: "/other",
				data: server
			});
      $('#otherResult').show(500);
      $('#otherGet').prop("disabled", true);
				return false;
    	});
      $('#alarmGet').click(function(){
        server = "day=" + $("#day").val() + "&name=" + $("#alarmName").val() + "&time=" + $("#alarmTime").val() + "&active=" + $('#active').is(":checked").toString() + "&repeat=" + $('#repeat').is(":checked").toString();
      $.ajax({
        url: "/alarmUpd",
        data: server
      });
        return false;
      });
      $('#alarmDel').click(function(){
        server = "day=&name=&time=&active=&repeat=";
      $.ajax({
        url: "/alarmDel",
        data: server
      });
        return false;
      });
	});

