#include "alarm.h"


SoftwareSerial secondarySerial(D1, D2);
DFMiniMp3<SoftwareSerial, Mp3Notify> mp3(secondarySerial);

Alarm::Alarm() {
}

void Alarm::start(){
  mp3.begin();
  mp3.reset();
  tracks = mp3.getTotalTrackCount();
  mp3.setVolume(/*_volume*/ 10);
}

void Alarm::loop(){
  if(isAlarm){
    if(!isStart){
      if(!_random){
        mp3.playMp3FolderTrack(4);
        isStart = true;
      }else{
        int rnd = random(1, tracks);
        Serial.println("playing " + String(rnd) + " track on sd");
        mp3.playMp3FolderTrack(rnd);
        isStart = true;
      }
    }
   }else{
     mp3.loop(); 
   }
 }

void Alarm::handle(String h, String m, String weekday){
  if(h.length() < 2) h = "0" + h;
  if(m.length() < 2) m = "0" + m;
  String thisTime = h+":"+m;
  int ned;

  if(weekday == "MON") ned = 0;
  if(weekday == "TUE") ned = 1;
  if(weekday == "WED") ned = 2;
  if(weekday == "THU") ned = 3;
  if(weekday == "FRI") ned = 4;
  if(weekday == "SAT") ned = 5;
  if(weekday == "SUN") ned = 6;
  
  if(!isAlarm){
    sec_to_end = 0;
    switch(ned){
      case 0:
        if(thisTime == _time[ned] && _active[ned]){
          if(!_repeat[ned]) _active[ned] = false;
          isAlarm = true;
        }
      break; // спарсить день недели
      case 1:
      if(thisTime == _time[ned] && _active[ned]){
        if(!_repeat[ned]) _active[ned] = false;
          isAlarm = true;
      }
      break;
      case 2:
      if(thisTime == _time[ned] && _active[ned]){
        if(!_repeat[ned]) _active[ned] = false;
          isAlarm = true;
      }
      break;
      case 3:
      if(thisTime == _time[ned] && _active[ned]){
        if(!_repeat[ned]) _active[ned] = false;
          isAlarm = true;
      }
      break;
      case 4:
      if(thisTime == _time[ned] && _active[ned]){
        if(!_repeat[ned]) _active[ned] = false;
          isAlarm = true;
      }
      break;
      case 5:
      if(thisTime == _time[ned] && _active[ned]){
        if(!_repeat[ned]) _active[ned] = false;
          isAlarm = true;
      }
      break;
      case 6:
      if(thisTime == _time[ned] && _active[ned]){
        if(!_repeat[ned]) _active[ned] = false;
          isAlarm = true;
      }
      break;
    }
  }else{
    if(sec_to_end < (alarm_duration + 1)){
      sec_to_end++;
    }else{
      isAlarm = false; isStart = false;
      saveAlarm();
      mp3.reset();
      Serial.println("Alarm is the end.");
    }
  }
}
