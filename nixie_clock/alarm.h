#ifndef Alarm_h
#define Alarm_h

#include <Arduino.h>
#include <DFMiniMp3.h>
#include <SoftwareSerial.h>

extern String _name[];
extern bool _active[];
extern bool _repeat[];
extern String _time[];

extern int alarm_duration;
extern int _volume;
extern bool _random;

extern void saveAlarm();

#define PIN_BUSY D0

class Alarm {
  public:
    Alarm();
    void handle(String, String, String);
    void start();
    void loop();
  private:
  int tracks;
  bool isAlarm = false;
  bool isStart = false;
  int sec_to_end;
    
};
class Mp3Notify
{
public:
  static void OnError(uint16_t errorCode)
  {
    // see DfMp3_Error for code meaning
    Serial.println();
    Serial.print("Com Error ");
    Serial.println(errorCode);
  }

  static void OnPlayFinished(uint16_t globalTrack)
  {
    Serial.println();
    Serial.print("Play finished for #");
    Serial.println(globalTrack);   
  }

  static void OnCardOnline(uint16_t code)
  {
    Serial.println();
    Serial.print("Card online ");
    Serial.println(code);     
  }

  static void OnCardInserted(uint16_t code)
  {
    Serial.println();
    Serial.print("Card inserted ");
    Serial.println(code); 
  }

  static void OnCardRemoved(uint16_t code)
  {
    Serial.println();
    Serial.print("Card removed ");
    Serial.println(code);  
  }
};
#endif
