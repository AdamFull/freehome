#ifndef Nixiedisp_h
#define Nixiedisp_h

#include <Arduino.h>

#define maxRegs 8

class display{
  public:
  display(int,int,int);
  void show(int nums[]);
  void setDelay(int delSec);
  private:
  void clearRegisters(void);
  void setRegisterPin(int, boolean);
  int _numOfPins;
  void writeRegisters(void); 
  boolean _registers[maxRegs*8];
  void setNumber(int num);
  int _DS; int _ST_CP; int _SH_CP;
  int _delSec = 1;
  int _startTime;
};

/*class Nixdisp {
  public:
    Nixdisp(int out1, int out2, int out4, int out8);
    void begin(byte segments[]);//initialise display
    void show(int nums[]);//Show numbers (1-6)
    void setDelay(int delSec);//Set update interval (millis) default 1ms
  private:
    void setNumber(int num);
    int _out1; int _out2; int _out4; int _out8;
    int _key1; int _key2; int _key3; int _key4; int _key5; int _key6;
    byte _segments;
    int _delSec = 1;
    int _startTime;
};*/
/*class NixSN74HC595 {
  public:
  NixSN74HC595(int out1, int out2, int out4, int out8);
  void begin(int segments, int DS, int ST_CP, int SH_CP, int regsCount);
  void show(int nums[]);
  void setDelay(int delSec);
  private:
  void clearRegisters(void);
  void setRegisterPin(int, boolean);
  int _numOfPins;
  void writeRegisters(void); 
  boolean _registers[maxRegs*8];
  void setNumber(int num);
  int _out1; int _out2; int _out4; int _out8;
  int _DS; int _ST_CP; int _SH_CP;
  int _segments;
  int _delSec = 1;
  int _startTime;
};*/
#endif
