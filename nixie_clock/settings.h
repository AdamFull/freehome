#ifndef Settings_h
#define Settings_h

#include "Arduino.h"
#include "ESP8266WebServer.h"
#include "ESP8266WiFi.h"
#include "WiFiClient.h"
#include "ESP8266mDNS.h"
#include "ESP8266SSDP.h"
#include "ArduinoJson.h"
#include "FS.h"

extern char *_SSDP_name;
extern char *_ssid;  //  your network SSID (name) RT-GPON-80AE
extern char *_pass;       // your network password R3FC7XDA
extern String _weatherKey; //eac0b755302dadb29a35aec3273c271b
extern String _weatherLang; //&lang=ru
extern String _cityID; //472045
extern int _utc; //3
extern String _ip;
extern int del;
extern long updateInf;
extern String jsonConfig;

extern String _name[];
extern bool _active[];
extern bool _repeat[];
extern String _time[];

extern int _volume;

extern float temp;
extern int humidity;
extern int pressure;
extern float windSpeed;


class ServerH {
  public:
    ServerH();
    void setup(void);
    void load();
    void save();
    void startHttp();
    void handle();
  private:
};
#endif
//Сделать подключение и другую дичь здесь
